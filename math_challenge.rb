class MathChallenge
  MATH_OPERATORS = %W(+ - / *).freeze

  def call
    loop do
      get_equation

      calculate_equation
    end
  end

  def get_equation
    printf "Please Enter your equation => "

    @input = gets.chomp.to_s
  end

  def calculate_equation
    operands    = input.split(%r{(\+)|(\-)|(\/)|(\*)})
    answer      = 0
    first_digit = 0
    operator    = nil

    operands.each_with_index do |operand, index|
      next first_digit = operand.to_f if index == 0
      next operator    = operand if MATH_OPERATORS.include?(operand)

      other_digits = operand.to_f

      if index == 2
        puts "#{first_digit} #{operator} #{other_digits}"

        answer = first_digit.send(operator, other_digits)
      else
        puts "#{answer} #{operator} #{other_digits}"

        answer = answer.send(operator, other_digits)
      end

      puts "%.1f" % answer
    end
  end

  private

  attr_reader :input
end

perform = MathChallenge.new

perform.call