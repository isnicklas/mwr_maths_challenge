#### MWR_MATHS_CHALLENGE
_ Attempted by Isaac E. Nicklas-Coker_

#### HOW TO RUN APPLICATION

- Please enter `irb` in terminal

- load application using the path to `math_challenge_rb`
(`load "path_to/math_challenge.rb"`)

- This should display `Enter your equation => `

- Enjoy testing my test :-)
